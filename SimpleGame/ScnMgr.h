#pragma once
#include "Renderer.h"
#include "G_Object.h"
#include "Globals.h"
#include"Physics.h"
#include"Sound.h"
class ScnMgr
{
public:
	ScnMgr();
	~ScnMgr();

	void Update(float elapsedInSec);
	void RenderScene();
	void DoGarbageCollection();
	unsigned int AddObject(
		float x, float y, float z, 
		float sx, float sy, float sz, 
		float r, float g, float b, float a,
		float vx, float vy, float vz,
		float mass ,
		float fricCoef,
		int type,
		float hp);

	void DeleteObject(int idx);

	void KeyDownInput(unsigned char key, int x, int y);
	void KeyUpInput(unsigned char key, int x, int y);

	void SpecialKeyDownInput(int key, int x, int y);
	void SpecialKeyUpInput(int key, int x, int y);

private:
	Renderer* m_Renderer = NULL;
	Sound* m_Sound = NULL;
	Physics* m_Physics = NULL;
	G_Object* m_Testobj = NULL;
	G_Object* m_obj[MAX_OBJ_COUNT];//앞으로 사용하게될 리소스 툴 ->캐릭터 등등제작
	int m_TestChar[10];

	bool m_keyW = false;
	bool m_keyA = false;
	bool m_keyS = false;
	bool m_keyD = false;
	bool m_KeySP = false;



	bool m_keyUp = false;
	bool m_keyLeft = false;
	bool m_keyDown = false;
	bool m_keyRight = false;

};

