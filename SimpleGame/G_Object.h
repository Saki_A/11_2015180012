#pragma once
class G_Object
{
public:
	G_Object();
	~G_Object();

	//void SetSize(float sx, float sy, float sz);
	//void GetSize(float* sx, float* sy, float* sz);

	//void SetLocation(float x, float y, float z);
	//void GetLocation(float* x, float* y, float* z);

	void Update(float elapsedInSec);
	void AddForce(float x, float y, float z, float eTime);

	void SetColor(float r, float g, float b,float a);
	void GetColor(float *r, float *g, float *b, float *a);

	void SetPos(float posx, float posy, float posz);
	void GetPos(float* posx, float* posy, float* posz);

	void Setmass(float mass);
	void Getmass(float* mass);

	void SetVel(float velx, float vely, float velz);
	void GetVel(float* velx, float* vely, float* velz);

	void SetAcc(float accx, float accy, float accz);
	void GetAcc(float* accx, float* accy, float* accz);

	void SetVol(float volx, float voly, float volz);
	void GetVol(float* volx, float* voly, float* volz);

	void SetFricCoef(float Coef);
	void GetFricCoef(float* Coef);

	void SetType(int type);
	void GetType(int* type);

	void SetTexID(int id);
	void GetTexID(int* id);

	void SetParentObj(G_Object* parent);
	G_Object* GetParentObj();
	bool IsAncestor(G_Object* obj);

	void InitPhysics();

	bool CanShootBullet();
	void ResrtBulletCoolTime();

	void GetHP(float* hp);
	void SetHP(float hp);
	void Damage(float damage);

private:
	//float m_x, m_y, m_z=0; //location
	//float m_sx, m_sy, m_sz=0; //size

	float m_posX, m_posY, m_posZ = 0; //position 위치
	float m_mass = 0; //mass 무게
	float m_velX, m_velY, m_velZ = 0;//velocity 속도
	float m_accX, m_accY, m_accZ = 0;//acceleration 가속도
	float m_volX, m_volY, m_volZ = 0;//volume 부피

	float m_r, m_g, m_b, m_a=0;//color 색
	float m_fricCoef; //friction 

	int m_type;	//object type

	int m_texID; //texID

	float m_healthPoint;

	G_Object* m_parent = NULL;


	float m_remainingCoolTime =0.f;
	float m_CurrentCoolTime = 0.2f;


};

