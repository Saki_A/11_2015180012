#include "stdafx.h"
#include "G_Object.h"
#include "Globals.h"
#include <math.h>
#include <float.h>
G_Object::G_Object()
{
	InitPhysics();
}
G_Object::~G_Object()
{

}

bool G_Object::CanShootBullet()
{
	if (m_remainingCoolTime < FLT_EPSILON)
	{
		return true;
	}
	return false;
}

void G_Object::ResrtBulletCoolTime()
{
	m_remainingCoolTime = m_CurrentCoolTime;
}

void G_Object::Update(float elapsedInSec)
{
	//reduce remaining cool
	m_remainingCoolTime -= elapsedInSec;


	////////////////Apply friction///////////////////////////
	float nForce = m_mass * GRAVITY;//scalar 수직항력 크기
	float fForce = m_fricCoef * nForce; //scalar 마찰력
	float velSize = sqrtf(m_velX * m_velX + m_velY * m_velY + m_velZ*m_velZ);//속도사이즈
	//예외처리
	if (velSize > 0.f)
	{
		float fDirX = -1.f * m_velX / velSize; //x축에 반대방향 가속도
		float fDirY = -1.f * m_velY / velSize; //마찰력의 방향
		fDirX = fDirX * fForce;
		fDirY = fDirY * fForce;
		float fAccX = fDirX / m_mass;//가속도
		float fAccY = fDirY / m_mass;//가속도
		float newX = m_velX + fAccX * elapsedInSec;
		float newY = m_velY + fAccY * elapsedInSec;
		if (newX * m_velX < 0.f)
		{
			m_velX = 0.f;
		}
		else
		{
			m_velX = newX;
		}

		if (newY * m_velY < 0.f)
		{
			m_velY = 0.f;
		}
		else
		{
			m_velY = newY;
		}

		//점프용 z축
		m_velZ = m_velZ - GRAVITY * elapsedInSec; //z축 속도업데이트


	}
	
	/////////////////////////////////////////////////////////
	m_posX = m_posX + m_velX* elapsedInSec;
	m_posY = m_posY + m_velY * elapsedInSec;
	m_posZ = m_posZ + m_velZ * elapsedInSec;

	if (m_posZ < FLT_EPSILON)
	{
		m_posZ = 0.f;
		m_velZ = 0.f;
	}
}

void G_Object::AddForce(float x, float y, float z,float eTime)
{
	float accX, accY, accZ;
	accX = accY = accZ = 0.f;
	
		accX = x / m_mass;
		accY = y / m_mass;
		accZ = z / m_mass;

		m_velX = m_velX + accX * eTime;
		m_velY = m_velY + accY * eTime;
		m_velZ = m_velZ + accZ * eTime;
	
}
/*
void G_Object::SetSize(float sx, float sy, float sz)
{
	m_sx = sx;
	m_sy = sy;
	m_sz = sz;
}
void G_Object::GetSize(float* sx, float* sy, float* sz)
{
	*sx = m_sx;
	*sy = m_sy;
	*sz = m_sz;
}

void G_Object::SetLocation(float x, float y, float z)
{
	m_x = x;
	m_y = y;
	m_z = z;
}
void G_Object::GetLocation(float* x, float* y, float* z)
{
	*x = m_x;
	*y = m_y;
	*z = m_z;

}
*/



void G_Object::SetPos(float posx, float posy, float posz)
{
	m_posX = posx;
	m_posY = posy;
	m_posZ = posz;
}

void G_Object::GetPos(float* posx, float* posy, float* posz)
{

	*posx = m_posX;
	*posy = m_posY;
	*posz = m_posZ;
}

void G_Object::Setmass(float mass)
{
	m_mass = mass;
}

void G_Object::Getmass(float* mass)
{
	*mass = m_mass;
}

void G_Object::SetVel(float velx, float vely, float velz)
{
	m_velX = velx;
	m_velY = vely;
	m_velZ = velz;
}

void G_Object::GetVel(float* velx, float* vely, float* velz)
{
	*velx = m_velX;
	*vely = m_velY;
	*velz = m_velZ;
}

void G_Object::SetAcc(float accx, float accy, float accz)
{
	m_accX = accx;
	m_accY = accy;
	m_accZ = accz;
}

void G_Object::GetAcc(float* accx, float* accy, float* accz)
{
	*accx = m_accX;
	*accy = m_accY;
	*accz = m_accZ;
}

void G_Object::SetVol(float volx, float voly, float volz)
{
	m_volX = volx;
	m_volY = voly;
	m_volZ = volz;
}

void G_Object::GetVol(float* volx, float* voly, float* volz)
{
	*volx = m_volX;
	*voly = m_volY;
	*volz = m_volZ;
}

void G_Object::SetFricCoef(float Coef)
{
	m_fricCoef = Coef;
}

void G_Object::GetFricCoef(float* Coef)
{
	*Coef = m_fricCoef;
}

void G_Object::SetType(int type)
{
	m_type = type;
}

void G_Object::GetType(int* type)
{
	*type = m_type;
}

void G_Object::SetTexID(int id)
{
	m_texID = id;
}

void G_Object::GetTexID(int* id)
{
	*id = m_texID;
}

void G_Object::SetParentObj(G_Object* parent)
{
	m_parent = parent;
}

G_Object* G_Object::GetParentObj()
{
	return m_parent;
}

bool G_Object::IsAncestor(G_Object* obj)
{
	if (obj != NULL)
	{
		if (obj == m_parent)
		{
			return true;
		}
		
	}
	
	
	return false;
}



void G_Object::InitPhysics()
{
	float m_posX = 0.f, m_posY = 0.f, m_posZ = 0.f; //position 위치
	float m_mass = 0.f; //mass 무게
	float m_velX = 0.f, m_velY = 0.f, m_velZ = 0.f;//velocity 속도
	float m_accX = 0.f, m_accY = 0.f, m_accZ = 0.f;//acceleration 가속도
	float m_volX = 0.f, m_volY = 0.f, m_volZ = 0.f;//volume 부피

	float m_r = 0.f, m_g = 0.f, m_b = 0.f, m_a = 0.f;//color 색
	m_fricCoef = 0.f;//마찰계수
}



void G_Object::SetColor(float r, float g, float b, float a)
{
	m_r = r;
	m_g = g;
	m_b = b;
	m_a = a;
}
void G_Object::GetColor(float* r, float* g, float* b, float* a)
{
	*r = m_r;
	*g = m_g;
	*b = m_b;
	*a = m_a;
}


void G_Object::GetHP(float* hp)
{
	*hp = m_healthPoint;
}
void G_Object::SetHP(float hp)
{
	m_healthPoint = hp;
}
void G_Object::Damage(float damage)
{
	m_healthPoint = m_healthPoint - damage;
}