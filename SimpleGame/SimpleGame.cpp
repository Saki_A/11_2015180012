/*
Copyright 2017 Lee Taek Hee (Korea Polytech University)

This program is free software: you can redistribute it and/or modify
it under the terms of the What The Hell License. Do it plz.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY.
*/

#include "stdafx.h"
#include <iostream>
#include "Dependencies\glew.h"
#include "Dependencies\freeglut.h"

#include "ScnMgr.h"
#include<math.h>
//Renderer *g_Renderer = NULL;
ScnMgr* g_ScnMgr = NULL;
int g_PrevTime = 0;

void RenderScene(int temp)
{
	int currTime = glutGet(GLUT_ELAPSED_TIME);
	int elapsedTime = currTime - g_PrevTime;
	if (elapsedTime > 150)
	{
		elapsedTime = 10;
	}
	float elapsedTimeInSec = (float)elapsedTime / 1000.f;
	
	g_PrevTime = currTime;

	std::cout << "elapsed time : " << elapsedTime << std::endl;


	g_ScnMgr->Update(elapsedTimeInSec);
	g_ScnMgr->RenderScene();
	g_ScnMgr->DoGarbageCollection();
	glutSwapBuffers();

	glutTimerFunc(10, RenderScene, 0);
}

void Display(void)
{
}
void Idle(void)
{
	
}

void MouseInput(int button, int state, int x, int y)
{
	
}

void KeyDownInput(unsigned char key, int x, int y)
{
	g_ScnMgr->KeyDownInput(key, x, y);
}
void KeyUpInput(unsigned char key, int x, int y)
{
	g_ScnMgr->KeyUpInput(key, x, y);
}

void SpecialKeyDownInput(int key, int x, int y)
{
	g_ScnMgr->SpecialKeyDownInput(key, x, y);
}

void SpecialKeyUpInput(int key, int x, int y)
{
	g_ScnMgr->SpecialKeyUpInput(key, x, y);
}

int main(int argc, char **argv)
{
	// Initialize GL things
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowPosition(0, 0);
	glutInitWindowSize(1000, 700);
	glutCreateWindow("Game Software Engineering KPU");

	glewInit();
	if (glewIsSupported("GL_VERSION_3_0"))
	{
		std::cout << " GLEW Version is 3.0\n ";
	}
	else
	{
		std::cout << "GLEW 3.0 not supported\n ";
	}
	//추가를 할때 이 밑에 추가를 해야 오류가 안난다


	g_ScnMgr = new ScnMgr();
	glutDisplayFunc(Display);
	glutIdleFunc(Idle);

	glutKeyboardFunc(KeyDownInput); //key down event callback 눌리는순간
	glutKeyboardUpFunc(KeyUpInput); //key up event callback 때지는순간

	glutMouseFunc(MouseInput);
	glutSpecialFunc(SpecialKeyDownInput);
	glutSpecialUpFunc(SpecialKeyUpInput);

	g_PrevTime = glutGet(GLUT_ELAPSED_TIME);
	glutTimerFunc(10, RenderScene, 0);

	glutMainLoop();

	delete g_ScnMgr;
    return 0;
}

