#include "stdafx.h"
#include "ScnMgr.h"
#include "Dependencies\freeglut.h"
#include <float.h>

//캐릭터
int Hero_Animation = -1;
int Enemy_Animation = -1;

//파티클
int FlowerParticleTex = -1;
int TitleParticleTex = -1;
int Title_particle = -1;
int Main_particle = -1;

//배경화면
int Game_Title_BG = -1;
int Game_Main_BG = -1;
int Game_Lose_BG = -1;
int Game_Win_BG = -1;

int Player_Bullet = -1;
int Enemy_Bullet = -1;
//BGM
int Game_BGM = -1;
int Main_BGM = -1;
int Lose_BGM = -1;
int Win_BGM = -1;
int g_FIRE = -1;

//UI
int Center_UI = -1;
int Hina_Face_E = -1;
int Hina_Face_G = -1;
int Hina_Face_B = -1;
int Rumia_Face_E = -1;
int Rumia_Face_G = -1;
int Rumia_Face_B = -1;
enum Face {Excellent,Good,Bad};
static int Hina_Face = Face::Excellent;
int Rumia_Face = Face::Excellent;
//


enum Game_Seen {Title,Help,Main,Lose,Win};
int Seen = Game_Seen::Title;

ScnMgr::ScnMgr()//생성자
{
	// Initialize Renderer
	m_Renderer = new Renderer(1000, 800); //935X690 픽셀로만들었다는것
	if (!m_Renderer->IsInitialized())
	{
		std::cout << "Renderer could not be initialized.. \n";
	}

	//Init Sound
	m_Sound = new Sound();

	m_Physics = new Physics();
	//Initialize Obj
	for (int i = 0; i < MAX_OBJ_COUNT; i++)
	{
		m_obj[i] = NULL;
	}

	//사운드
	Main_BGM = m_Sound->CreateBGSound("./Sounds/Main_BGM.mp3"); //Title
	Game_BGM = m_Sound->CreateBGSound("./Sounds/Game_BGM.mp3"); //Game
	Lose_BGM=m_Sound->CreateBGSound("./Sounds/Lose_BGM.mp3");
	Win_BGM=m_Sound->CreateBGSound("./Sounds/Win_BGM.mp3");
	g_FIRE = m_Sound->CreateShortSound("./Sounds/fire.mp3"); //shot
	//텍스쳐
	Hero_Animation = m_Renderer->GenPngTexture("./Textures/Hero.png");
	Enemy_Animation = m_Renderer->GenPngTexture("./Textures/Enemy.png");
	FlowerParticleTex = m_Renderer->GenPngTexture("./Textures/Flower.png");
	TitleParticleTex = m_Renderer->GenPngTexture("./Textures/Title_P.png");
	Player_Bullet = m_Renderer->GenPngTexture("./Textures/Player_Bullet.png"); //shot
	Enemy_Bullet = m_Renderer->GenPngTexture("./Textures/Enemy_Bullet.png"); //shot
	//배경화면
	Game_Title_BG = m_Renderer->GenPngTexture("./Textures/Title.png");
	Game_Main_BG = m_Renderer->GenPngTexture("./Textures/Fild.png");
	Game_Lose_BG = m_Renderer->GenPngTexture("./Textures/Lose.png");
	Game_Win_BG = m_Renderer->GenPngTexture("./Textures/Win.png");
	//UI
	Center_UI = m_Renderer->GenPngTexture("./Textures/Center.png");
	Hina_Face_E = m_Renderer->GenPngTexture("./Textures/Hina_Face1.png");
	Hina_Face_G = m_Renderer->GenPngTexture("./Textures/Hina_Face2.png");
	Hina_Face_B = m_Renderer->GenPngTexture("./Textures/Hina_Face3.png");
	Rumia_Face_E = m_Renderer->GenPngTexture("./Textures/Rumia_Face1.png");
	Rumia_Face_G = m_Renderer->GenPngTexture("./Textures/Rumia_Face2.png");
	Rumia_Face_B = m_Renderer->GenPngTexture("./Textures/Rumia_Face3.png");

	m_Sound->PlayBGSound(Main_BGM, true, 0.4f);
	//Create Hero
	//m_obj[HERO_ID] = new G_Object();
	//m_obj[HERO_ID]->SetPos(0.f, 0.f, 0.f);
	//m_obj[HERO_ID]->SetVol(0.5, 0.5, 0.5); //크기조절
	//m_obj[HERO_ID]->SetColor(1, 1, 1, 1);
	//m_obj[HERO_ID]->SetVel(0, 0, 0); //속도
	//m_obj[HERO_ID]->Setmass(1);
	//m_obj[HERO_ID]->SetFricCoef(0.7f);
	//m_obj[HERO_ID]->SetType(TYPE_HERO);
	//m_obj[HERO_ID]->SetHP(100); //HP
	//m_obj[HERO_ID]->SetTexID(Hero_Animation);

	//Create Enemy
	//m_obj[Enemy_Boss] = new G_Object();
	//m_obj[Enemy_Boss]->SetPos(5, 0, 0);
	//m_obj[Enemy_Boss]->SetVol(1,1,1); //크기조절
	//m_obj[Enemy_Boss]->SetColor(1, 1, 1, 1);
	//m_obj[Enemy_Boss]->SetVel(0, 0, 0); //속도
	//m_obj[Enemy_Boss]->Setmass(1);
	//m_obj[Enemy_Boss]->SetFricCoef(0.6f);
	//m_obj[Enemy_Boss]->SetType(TYPE_ENEMY);
	//m_obj[Enemy_Boss]->SetHP(150); //HP
	//m_obj[Enemy_Boss]->SetTexID(Enemy_Animation);
	
	
	Main_particle = m_Renderer->CreateParticleObject(
		500, //갯수
		-1500,-400, //위치
		1500,400,
		5,5,
		10,10,
		-20,-20,//생성당시속도
		20,20 //사방으로 나오는 속도
	);

	Title_particle = m_Renderer->CreateParticleObject(
		60, //갯수
		-500, 300, //위치
		500, 400,
		10, 10,
		15, 15,
		-20, -20,//생성당시속도
		20, 20 //사방으로 나오는 속도
	);

}

ScnMgr::~ScnMgr()
{
	if (m_Renderer != NULL)
	{
		delete m_Renderer;
		m_Renderer = NULL;
	}

	if (m_Physics != NULL)
	{
		delete m_Physics;
		m_Physics = NULL;
	}

}

void ScnMgr::Update(float elapsedInSec)
{
	//Character contol : Hero
	//std::cout << "W:" << m_keyW << ", A:" << m_keyA << ", S:" << m_keyS << ",D:" << m_keyD << std::endl;
	//std::cout << "Up:" << m_keyUp << ", Left:" << m_keyLeft << ", Down:" << m_keyDown << ",Right:" << m_keyRight << std::endl;
	

	if (Seen == Game_Seen::Main)
	{
	
		
		if (m_obj[HERO_ID] == NULL)
		{
			m_obj[HERO_ID] = new G_Object();
			m_obj[HERO_ID]->SetPos(0.f, 0.f, 0.f);
			m_obj[HERO_ID]->SetVol(0.5, 0.5, 0.5); //크기조절
			m_obj[HERO_ID]->SetColor(1, 1, 1, 1);
			m_obj[HERO_ID]->SetVel(0, 0, 0); //속도
			m_obj[HERO_ID]->Setmass(1);
			m_obj[HERO_ID]->SetFricCoef(0.7f);
			m_obj[HERO_ID]->SetType(TYPE_HERO);
			m_obj[HERO_ID]->SetHP(100); //HP
			m_obj[HERO_ID]->SetTexID(Hero_Animation);
		}
		if (m_obj[Enemy_Boss] == NULL)
		{
			m_obj[Enemy_Boss] = new G_Object();
			m_obj[Enemy_Boss]->SetPos(5, 0, 0);
			m_obj[Enemy_Boss]->SetVol(1, 1, 1); //크기조절
			m_obj[Enemy_Boss]->SetColor(1, 1, 1, 1);
			m_obj[Enemy_Boss]->SetVel(0, 0, 0); //속도
			m_obj[Enemy_Boss]->Setmass(1);
			m_obj[Enemy_Boss]->SetFricCoef(0.6f);
			m_obj[Enemy_Boss]->SetType(TYPE_ENEMY);
			m_obj[Enemy_Boss]->SetHP(150); //HP
			m_obj[Enemy_Boss]->SetTexID(Enemy_Animation);
		}
		
		float fx, fy, fz;
		fx = fy = fz = 0.f;
		float fAmount = 10.f; //캐릭터 속도조절	
		if (m_keyW)
		{
			fy += 1.f;
		}
		if (m_keyS)
		{
			fy -= 1.f;
		}
		if (m_keyA)
		{
			fx -= 1.f;
		}
		if (m_keyD)
		{
			fx += 1.f;
		}
		if (m_KeySP)
		{
				fz += 1.f;
		}
		//Add control force to Hero
		float fsize = sqrtf(fx * fx + fy * fy);
		if (fsize >= FLT_EPSILON)
		{
			fx /= fsize;
			fy /= fsize;
			fx *= fAmount;
			fy *= fAmount;
			
			m_obj[HERO_ID]->AddForce(fx, fy, 0.f, elapsedInSec);

		}
		//점프
		if (fz > FLT_EPSILON)
		{
			float x, y, z;
  			x = y = z = 0.f;
			m_obj[HERO_ID]->GetPos(&x, &y, &z);

			if (z <= FLT_EPSILON)
			{
 				fz *= fAmount * 15.f;
 				m_obj[HERO_ID]->AddForce(0.f, 0.f, fz, elapsedInSec);
			}
		}
		//보스 움직임
	
		{
			float Px, Py, Pz;
			m_obj[HERO_ID]->GetPos(&Px, &Py, &Pz);
			float BPx, BPy, BPz;
			m_obj[Enemy_Boss]->GetPos(&BPx, &BPy, &BPz);

			float Bfx, Bfy, Bfz;
			Bfx = Bfy = Bfz = 0.f;


			if (Py > BPy)
			{
				//BPy += 0.01f;
				Bfy += 1.f;
			}
			if (Py <= BPy)
			{
				//BPy -= 0.01f;
				Bfy -= 1.f;
			}
			if (Px <= BPx)
			{
				//BPx -= 0.01f;
				Bfx -= 1.f;
			}
			if (Px > BPx)
			{
				//BPx += 0.01f;
				Bfx += 1.f;
			}
			//m_obj[Enemy_Boss]->SetPos(BPx, BPy, 0.f);
			float Bfsize = sqrtf(Bfx * Bfx + Bfy * Bfy);
			float BfAmount = 10.f; //캐릭터 속도조절
			if (Bfsize >= FLT_EPSILON)
			{
			
				Bfx /= Bfsize;
				Bfy /= Bfsize;
				Bfx *= BfAmount;
				Bfy *= BfAmount;
				
				m_obj[Enemy_Boss]->AddForce(Bfx, Bfy, 0.f, elapsedInSec);
			}
		}
	
		
		//Fire bullets
		if (m_obj[HERO_ID]->CanShootBullet())
			//if (m_keyUp | m_keyDown | m_keyLeft | m_keyRight)
		{
			float bulletVel = 5.f;
			float vBulletX, vBulletY, vBulletZ;//불릿 속도
			vBulletX = vBulletY = vBulletZ = 0.f;
			if (m_keyUp) vBulletY += 1.f;
			if (m_keyDown) vBulletY -= 1.f;
			if (m_keyLeft) vBulletX -= 1.f;
			if (m_keyRight) vBulletX += 1.f;

			float vBulletSize = sqrtf(vBulletX * vBulletX + vBulletY * vBulletY + vBulletZ * vBulletZ);
			if (vBulletSize > FLT_EPSILON)
			{
				//Create bullet object
				vBulletX /= vBulletSize;
				vBulletY /= vBulletSize;
				vBulletZ /= vBulletSize;
				vBulletX *= bulletVel;
				vBulletY *= bulletVel;
				vBulletZ *= bulletVel;

				float hx, hy, hz;
				float hvx, hvy, hvz;
				m_obj[HERO_ID]->GetPos(&hx, &hy, &hz);
				m_obj[HERO_ID]->GetVel(&hvx, &hvy, &hvz);

				vBulletX += hvx;
				vBulletY += hvy;
				vBulletZ += hvz;

				//총알
				int P_Bullet = AddObject(
					hx, hy+0.15, hz,
					0.25, 0.25, 0.25,
					1, 1, 1, 1,
					vBulletX, vBulletY, vBulletZ,
					1.f,
					0.6,
					TYPE_BULLET,
					5);
				m_obj[P_Bullet]->SetTexID(Player_Bullet);
				m_obj[P_Bullet]->AddForce(vBulletX, vBulletY, vBulletZ, 0);
				m_obj[P_Bullet]->SetParentObj(m_obj[HERO_ID]);
				m_obj[HERO_ID]->ResrtBulletCoolTime();
				m_Sound->PlayShortSound(g_FIRE, false, 1.f);

			}



		}
		//보스 총알
		
		if (m_obj[Enemy_Boss]->CanShootBullet())
			//if (m_keyUp | m_keyDown | m_keyLeft | m_keyRight)
		
		{
			float Px, Py, Pz;
			m_obj[HERO_ID]->GetPos(&Px, &Py, &Pz);
			float BPx, BPy, BPz;
			m_obj[Enemy_Boss]->GetPos(&BPx, &BPy, &BPz);

			float bulletVel = 5.f;
			float vBulletX, vBulletY, vBulletZ;//불릿 속도
			vBulletX = vBulletY = vBulletZ = 0.f;
			if (Px < BPx)
			{
				vBulletY += 1.f;
				vBulletX -= 1.f;
			}
			else if (Px > BPx)
			{
				vBulletY -= 1.f;
				vBulletX += 1.f;
			}
			float vBulletSize = sqrtf(vBulletX * vBulletX + vBulletY * vBulletY + vBulletZ * vBulletZ);
			if (vBulletSize > FLT_EPSILON)
			{
				//Create bullet object
				vBulletX /= vBulletSize;
				vBulletY /= vBulletSize;
				vBulletX *= bulletVel;
				vBulletY *= bulletVel;

				float hx, hy, hz;
				float hvx, hvy, hvz;
				m_obj[Enemy_Boss]->GetPos(&hx, &hy, &hz);
				m_obj[Enemy_Boss]->GetVel(&hvx, &hvy, &hvz);

				vBulletX += hvx;
				vBulletY += hvy;

				//총알
				{
				int B_Bullet = AddObject(
					hx, hy+0.15f , hz,
					0.25, 0.25, 0.25,
					1, 1, 1, 1,
					vBulletX*1.3f, 0.f, 0.f,
					1.f,
					0.6,
					TYPE_BULLET,
					5);

				
					m_obj[B_Bullet]->SetTexID(Enemy_Bullet);
					m_obj[B_Bullet]->AddForce(vBulletX, 0.f, 0.f, 0);
					m_obj[B_Bullet]->SetParentObj(m_obj[Enemy_Boss]);

						
					
				
				}
				
				m_obj[Enemy_Boss]->ResrtBulletCoolTime();
				

			}
		}
		

		//Overlaptest
		for (int src = 0; src < MAX_OBJ_COUNT; src++)
		{

			for (int dst = src + 1; dst < MAX_OBJ_COUNT; dst++)
			{
				if (m_obj[src] != NULL && m_obj[dst] != NULL)
				{
					int src_type = -1;
					int dst_type = -1;
					m_obj[src]->GetType(&src_type);
					m_obj[dst]->GetType(&dst_type);
					if (m_Physics->IsOverlap(m_obj[src], m_obj[dst]) )
					{
						std::cout << "Collisin : (" << src << ", " << dst << ")" << std::endl;

						if (!m_obj[src]->IsAncestor(m_obj[dst]) && !m_obj[dst]->IsAncestor(m_obj[src])&& src_type != dst_type)
						{
							//Collisin시 할 행돌들을 밑에 기입
							//Process collision
							m_Physics->ProcessCollision(m_obj[src], m_obj[dst]);

							//damage
							float srcHP, dstHP;
							m_obj[src]->GetHP(&srcHP);
							m_obj[dst]->GetHP(&dstHP);
							m_obj[src]->Damage(dstHP);
							m_obj[dst]->Damage(srcHP);

							m_Sound->PlayShortSound(g_FIRE, false, 1.f);

						}
					}
				}
			}

		}


		//For all objs, call update func 모든오브젝트에 대해서
		for (int i = 0; i < MAX_OBJ_COUNT; i++)
		{
			if (m_obj[i] != NULL)
			{
				m_obj[i]->Update(elapsedInSec);
			}
		}

		//카메라 설정
		float x, y, z;
		m_obj[HERO_ID]->GetPos(&x, &y, &z);
		if (x >= -10 && x <= 10)
		{
			m_Renderer->SetCameraPos(x * 100, 0); //픽셀단위라서 곱해야댐
		}
		//맵밖으로나가면 중앙으로
		{
			if (x <= -15)
			{
				m_obj[HERO_ID]->SetPos(0.f, 0.f, z);

			}

			if (x >= 15)
			{
				m_obj[HERO_ID]->SetPos(0.f, 0.f, z);
			}

			if (y >= 3.75)
			{
				m_obj[HERO_ID]->SetPos(0.f, 0.f, z);
			}

			if (y <= -4.25)
			{
				m_obj[HERO_ID]->SetPos(0.f, 0.f, z);
			}

		}

		//적도 맵밖으로 나가면 중앙으로
		float Bx, By, Bz;
		m_obj[Enemy_Boss]->GetPos(&Bx, &By, &Bz);
		{
			if (Bx <= -15)
			{
				m_obj[Enemy_Boss]->SetPos(0.f, 0.f, 0.f);

			}

			if (Bx >= 15)
			{
				m_obj[Enemy_Boss]->SetPos(0.f, 0.f, 0.f);
			}

			if (By >= 3.75)
			{
				m_obj[Enemy_Boss]->SetPos(0.f, 0.f, 0.f);
			}

			if (By <= -4.25)
			{
				m_obj[Enemy_Boss]->SetPos(0.f, 0.f, 0.f);
			}


		}

		float H_hp,E_hp;
		m_obj[HERO_ID]->GetHP(&H_hp);
		if (H_hp < FLT_EPSILON)
		{
			Seen = Game_Seen::Lose;
			m_Renderer->SetCameraPos(0, 0); //픽셀단위라서 곱해야댐
			m_Sound->StopBGSound(Game_BGM);
			m_Sound->PlayBGSound(Lose_BGM, true, 0.4f);

			DeleteObject(HERO_ID);
		}

		m_obj[Enemy_Boss]->GetHP(&E_hp);
		if (E_hp < FLT_EPSILON)
		{
			Seen = Game_Seen::Win;
			m_Renderer->SetCameraPos(0, 0); //픽셀단위라서 곱해야댐
			m_Sound->StopBGSound(Game_BGM);
			m_Sound->PlayBGSound(Win_BGM, true, 0.4f);
			DeleteObject(Enemy_Boss);
		}
	}

}

void ScnMgr::RenderScene()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.0f, 0.3f, 0.3f, 1.0f);

	if (Seen == Game_Seen::Title)
	{
		//백그라운드 그리기
		m_Renderer->DrawGround(
			0.f, 0.f, 0.f,
			1000.f, 800.f, 0.f,
			1, 1, 1, 1,
			Game_Title_BG,
			1.f
		);

		static float pTime = 0.f;
		pTime += 0.016f;

		m_Renderer->DrawParticle(Title_particle,
			0, 0, 0,
			1,
			1, 1, 1, 1,
			-5, -30,
			TitleParticleTex,
			1, 
			pTime
		);

	}
	if (Seen == Game_Seen::Main)
	{
		//백그라운드 그리기
		m_Renderer->DrawGround(
			0.f, 0.f, 0.f,
			3000.f, 800.f, 0.f,
			1, 1, 1, 1,
			Game_Main_BG,
			1.f
		);

		//파티클그리기
		static float pTime = 0.f;
		pTime += 0.016f;

		m_Renderer->DrawParticle(Main_particle,
			0, 0, 0,
			1,
			1, 1, 1, 1,
			-30, -10,//가속도 //이거면 -50으로 쭉쭉 이동함
			FlowerParticleTex,
			1, //0~1  얼마나 그릴지 1=다그림
			pTime//점점퍼저나가는걸보고싶으면 시간을 중첩시킨다.
		);


		// Draw all m_objs
		for (int i = 0; i < MAX_OBJ_COUNT; i++)
		{
			if (m_obj[i] != NULL)
			{
				float x, y, z = 0; //location
				float sx, sy, sz = 0;//size
				float r, g, b, a = 0;//color
				m_obj[i]->GetPos(&x, &y, &z);
				m_obj[i]->GetVol(&sx, &sy, &sz); //미터단위
				m_obj[i]->GetColor(&r, &g, &b, &a);

				//그릴때만 단위변경하는것이다.
				//1미터 == 100센치미터 == 100픽셀
				x = x * 100.f;
				y = y * 100.f;
				z = z * 100.f; //픽셀단위로변경

				sx = sx * 100.f;
				sy = sy * 100.f;
				sz = sz * 100.f; //픽셀단위로변경


				//오브젝트 그릴떄 기본틀
				//m_Renderer->DrawSolidRect(x, y, z, sx, r, g, b, a); //픽셀단위
				int texID = -1;
				m_obj[i]->GetTexID(&texID);

				//게이지 생성
				int type = -1;
				float HPgauge = -1;
				m_obj[i]->GetHP(&HPgauge);
				m_obj[i]->GetType(&type);
				if (type == TYPE_HERO)
				{

					m_Renderer->DrawSolidRectGauge(
						x, y, z,
						0, sy / 2.f + 10, 0,
						sx, 5, 0,
						0, 0, 0, 1,
						Hero_MaxHP);



					m_Renderer->DrawSolidRectGauge(
						x, y, z,
						0, sy / 2.f + 10, 0,
						sx, 5, 0,
						1, 0, 0, 1,
						HPgauge);
				}
				else if (type == TYPE_ENEMY)
				{

					m_Renderer->DrawSolidRectGauge(
						x, y, z,
						0, sy / 2.f + 10, 0,
						sx, 5, 0,
						0, 0, 0, 1,
						Enemy_MaxHP*0.67);

					m_Renderer->DrawSolidRectGauge(
						x, y, z,
						0, sy / 2.f + 10, 0,
						sx, 5, 0,
						1, 0, 0, 1,
						HPgauge * 0.67);
				}

				//캐릭터 애니메이션
				if (i == HERO_ID)
				{
					static int temp = 0;
					static int Ctime = 0; //애니메이션 속도조절
					int iX = temp % 5;
					static int iY = 0;

					if (m_keyD == true)
					{
						iY = 0;
					}
					else if (m_keyA == true)
					{
						iY = 1;
					}
					m_Renderer->DrawTextureRectAnim(
						x, y, z,
						sx, sy, sz,
						r, g, b, a,
						texID,
						5,
						2,
						iX,
						iY);
				
					if (Ctime == 15)
					{
						temp++;
						Ctime = 0;
					}
					else
					{
						Ctime++;
					}

					if (temp > 2)
					{
						temp = 0;
					}
				}
				else if (i == TYPE_ENEMY)
				{
					static int temp = 0;
					static int Ctime = 0; //애니메이션 속도조절
					int iX = temp % 3;
					static int iY = 0;
					float Px, Py, Pz;
					m_obj[HERO_ID]->GetPos(&Px, &Py, &Pz);
					float BPx, BPy, BPz;
					m_obj[Enemy_Boss]->GetPos(&BPx, &BPy, &BPz);

					if (Px < BPx)
					{
						iY = 1;
					}
					else if (Px > BPx)
					{
						iY = 0;
					}
					m_Renderer->DrawTextureRectAnim(
						x, y, z,
						sx, sy, sz,
						r, g, b, a,
						texID,
						3,
						2,
						iX,
						iY);

					if (Ctime == 15)
					{
						temp++;
						Ctime = 0;
					}
					else
					{
						Ctime++;
					}

					if (temp > 2)
					{
						temp = 0;
					}
				}
				else
				{
					m_Renderer->DrawTextureRect(x, y, z, sx, sy, sz, r, g, b, a, texID); //픽셀단위
				}
			}

			//UI 그리기
			float x, y, z = 0; //location
			m_obj[HERO_ID]->GetPos(&x, &y, &z);
			
			m_Renderer->DrawGround(
				0.f, 0.f, 0.f,
				60.f, 60.f, 0.f,
				1, 1, 1, 1,
				Center_UI,
				0.9f
			);

			//플레이어 얼굴
			float P_HP;
			m_obj[HERO_ID]->GetHP(&P_HP);
			if (70 <= P_HP)
			{
				Hina_Face = Face::Excellent;
			}
			else if (35 <= P_HP)
			{
				Hina_Face = Face::Good;
			}
			else if (P_HP < 35)
			{
				Hina_Face = Face::Bad;
			}
			if (Hina_Face == 0)
			{
				if (x >= -10 && x <= 10)
				{
					m_Renderer->DrawGround(
						x * 100 - 430, 330.f, 0.f,
						140.f, 140.f, 0.f,
						1, 1, 1, 1,
						Hina_Face_E,
						0.9f
					);
				}
				else if (x < -10)
				{
					m_Renderer->DrawGround(
						-1430, 330.f, 0.f,
						140.f, 140.f, 0.f,
						1, 1, 1, 1,
						Hina_Face_E,
						0.9f
					);
				}
				else if (x > 10)
				{
					m_Renderer->DrawGround(
						570, 330.f, 0.f,
						140.f, 140.f, 0.f,
						1, 1, 1, 1,
						Hina_Face_E,
						0.9f
					);
				}
			}

			if (Hina_Face == 1)
			{
				if (x >= -10 && x <= 10)
				{
					m_Renderer->DrawGround(
						x * 100 - 430, 330.f, 0.f,
						140.f, 140.f, 0.f,
						1, 1, 1, 1,
						Hina_Face_G,
						0.9f
					);
				}
				else if (x < -10)
				{
					m_Renderer->DrawGround(
						-1430, 330.f, 0.f,
						140.f, 140.f, 0.f,
						1, 1, 1, 1,
						Hina_Face_G,
						0.9f
					);
				}
				else if (x > 10)
				{
					m_Renderer->DrawGround(
						570, 330.f, 0.f,
						140.f, 140.f, 0.f,
						1, 1, 1, 1,
						Hina_Face_G,
						0.9f
					);
				}
			}

			if (Hina_Face == 2)
			{
				if (x >= -10 && x <= 10)
				{
					m_Renderer->DrawGround(
						x * 100 - 430, 330.f, 0.f,
						140.f, 140.f, 0.f,
						1, 1, 1, 1,
						Hina_Face_B,
						0.9f
					);
				}
				else if (x < -10)
				{
					m_Renderer->DrawGround(
						-1430, 330.f, 0.f,
						140.f, 140.f, 0.f,
						1, 1, 1, 1,
						Hina_Face_B,
						0.9f
					);
				}
				else if (x > 10)
				{
					m_Renderer->DrawGround(
						570, 330.f, 0.f,
						140.f, 140.f, 0.f,
						1, 1, 1, 1,
						Hina_Face_B,
						0.9f
					);
				}
			}


			//보스얼굴
			float B_HP;
			m_obj[Enemy_Boss]->GetHP(&B_HP);
			if (100 <= B_HP)
			{
				Rumia_Face = Face::Excellent;
			}
			else if (50 <= B_HP)
			{
				Rumia_Face = Face::Good;
			}
			else if (B_HP < 50)
			{
				Rumia_Face = Face::Bad;
			}
			if (Rumia_Face == 0)
			{
				if (x >= -10 && x <= 10)
				{
					m_Renderer->DrawGround(
						x * 100 + 430, 330.f, 0.f,
						140.f, 140.f, 0.f,
						1, 1, 1, 1,
						Rumia_Face_E,
						0.9f
					);
				}
				else if (x < -10)
				{
					m_Renderer->DrawGround(
						-570, 330.f, 0.f,
						140.f, 140.f, 0.f,
						1, 1, 1, 1,
						Rumia_Face_E,
						0.9f
					);
				}
				else if (x > 10)
				{
					m_Renderer->DrawGround(
						1430, 330.f, 0.f,
						140.f, 140.f, 0.f,
						1, 1, 1, 1,
						Rumia_Face_E,
						0.9f
					);
				}
			}

			if (Rumia_Face == 1)
			{
				if (x >= -10 && x <= 10)
				{
					m_Renderer->DrawGround(
						x * 100 + 430, 330.f, 0.f,
						140.f, 140.f, 0.f,
						1, 1, 1, 1,
						Rumia_Face_G,
						0.9f
					);
				}
				else if (x < -10)
				{
					m_Renderer->DrawGround(
						-570, 330.f, 0.f,
						140.f, 140.f, 0.f,
						1, 1, 1, 1,
						Rumia_Face_G,
						0.9f
					);
				}
				else if (x > 10)
				{
					m_Renderer->DrawGround(
						1430, 330.f, 0.f,
						140.f, 140.f, 0.f,
						1, 1, 1, 1,
						Rumia_Face_G,
						0.9f
					);
				}
			}

			if (Rumia_Face == 2)
			{
				if (x >= -10 && x <= 10)
				{
					m_Renderer->DrawGround(
						x * 100 + 430, 330.f, 0.f,
						140.f, 140.f, 0.f,
						1, 1, 1, 1,
						Rumia_Face_B,
						0.9f
					);
				}
				else if (x < -10)
				{
					m_Renderer->DrawGround(
						-570, 330.f, 0.f,
						140.f, 140.f, 0.f,
						1, 1, 1, 1,
						Rumia_Face_B,
						0.9f
					);
				}
				else if (x > 10)
				{
					m_Renderer->DrawGround(
						1430, 330.f, 0.f,
						140.f, 140.f, 0.f,
						1, 1, 1, 1,
						Rumia_Face_B,
						0.9f
					);
				}
			}
		}

	}
	if (Seen == Game_Seen::Lose)
	{
		//백그라운드 그리기
		m_Renderer->DrawGround(
			0.f, 0.f, 0.f,
			1000.f, 800.f, 0.f,
			1, 1, 1, 1,
			Game_Lose_BG,
			1.f
		);
	}
	if (Seen == Game_Seen::Win)
	{
		//백그라운드 그리기
		m_Renderer->DrawGround(
			0.f, 0.f, 0.f,
			1000.f, 800.f, 0.f,
			1, 1, 1, 1,
			Game_Win_BG,
			1.f
		);
	}
}

void ScnMgr::DoGarbageCollection()
{
	if (Seen == Game_Seen::Main)
	{
		for (int i = 0; i < MAX_OBJ_COUNT; i++)
		{
			if (m_obj[i] == NULL)
			{
				continue;
			}

			int type = -1;
			m_obj[i]->GetType(&type);
			if (type == TYPE_BULLET)
			{
				//Check velocity size
				float vx, vy, vz;
				m_obj[i]->GetVel(&vx, &vy, &vz);
				float vSize = sqrtf(vx * vx + vy * vy + vz * vz);
				if (vSize < FLT_EPSILON)
				{
					DeleteObject(i);
					continue;
				}
			}
			float hp;
			m_obj[i]->GetHP(&hp);


			if (hp < FLT_EPSILON)
			{
				DeleteObject(i);
				continue;
			}
		}
	}
}


unsigned int ScnMgr::AddObject(float x, float y, float z,
	float sx, float sy, float sz,
	float r, float g, float b, float a,
	float vx, float vy, float vz,
	float mass,
	float fricCoef,
	int type,
	float hp)
{
	int idx = -1;
	for (int i = 0; i < MAX_OBJ_COUNT; i++)
	{
		if (m_obj[i] == NULL)
		{
			idx = i;
			break; //빈곳을 찾으면  빈곳을 정하고 나가기
		}
	}

	if (idx == -1)//빈공간이 없을경우
	{
		std::cout << "No more empty obj slot. " << std::endl;
		return -1;
	}

	//Test object class
	m_obj[idx] = new G_Object();
	m_obj[idx]->SetPos(x, y, z);
	m_obj[idx]->SetVol(sx, sy, sz);
	m_obj[idx]->SetColor(r, g, b, a);
	m_obj[idx]->SetVel(vx,vy,vz); //속도
	m_obj[idx]->Setmass(mass);
	m_obj[idx]->SetFricCoef(fricCoef);
	m_obj[idx]->SetType(type);
	m_obj[idx]->SetHP(hp);

	return idx;
}

void ScnMgr::DeleteObject(int idx)
{
	if (idx < 0)
	{
		std::cout << "input idx is negative" <<idx<< std::endl;
		return;
	}


	if (idx > MAX_OBJ_COUNT)
	{
		std::cout << "input idx is exceeds MAX_OBJ_COUNT" << idx << std::endl;
		return;
	}

	if (m_obj[idx]==NULL)
	{
		std::cout << "m_obj["<<idx<<"] is NULL "  << std::endl;
		return;
	}

	delete m_obj[idx];
	m_obj[idx]=NULL;
}

void ScnMgr::KeyDownInput(unsigned char key, int x, int y)
{
	if (key == 'w' || key == 'W')
	{
		m_keyW = true;
	}
	if (key == 'a' || key == 'A')
	{
		
		m_keyA = true;
		
	}
	if (key == 's' || key == 'S')
	{
		m_keyS = true;
	}
	if (key == 'd' || key == 'D')
	{
		m_keyD = true;
	}
	if (key == ' ')
	{
		m_KeySP = true;
	}

	if (key == 'g' || key == 'G')
	{
		if (Seen == Game_Seen::Title)
		{
			m_Sound->StopBGSound(Main_BGM);
			m_Sound->PlayBGSound(Game_BGM, true, 0.4f);
			//시작하기전에 싹다 초기화
			for (int i = 0; i < MAX_OBJ_COUNT; i++)
			{
				DeleteObject(i);
			}
			Seen = Game_Seen::Main;
		}
	}

	if (key == 'r' || key == 'R')
	{
		if (Seen == Game_Seen::Lose)
		{
			m_Sound->StopBGSound(Lose_BGM);
			m_Sound->PlayBGSound(Game_BGM, true, 0.4f);
			Seen = Game_Seen::Title;
		}

		else if (Seen == Game_Seen::Win)
		{
			m_Sound->StopBGSound(Win_BGM);
			m_Sound->PlayBGSound(Game_BGM, true, 0.4f);
			Seen = Game_Seen::Title;
		}
	}

}

void ScnMgr::KeyUpInput(unsigned char key, int x, int y)
{
	if (key == 'w' || key == 'W')
	{
		m_keyW = false;
	}
	if (key == 'a' || key == 'A')
	{
		m_keyA = false;
	}
	if (key == 's' || key == 'S')
	{
		m_keyS = false;
	}
	if (key == 'd' || key == 'D')
	{
		m_keyD = false;
	}
	if (key == ' ')
	{
		
		m_KeySP = false;
		
	}
}



void ScnMgr::SpecialKeyDownInput(int key, int x, int y)
{
	if (key == GLUT_KEY_UP)
	{
		m_keyUp = true;
	}
	if (key == GLUT_KEY_LEFT)
	{
		m_keyLeft = true;
	}
	if (key == GLUT_KEY_DOWN)
	{
		m_keyDown = true;
	}
	if (key == GLUT_KEY_RIGHT)
	{
		m_keyRight = true;
	}
}

void ScnMgr::SpecialKeyUpInput(int key, int x, int y)
{
	if (key == GLUT_KEY_UP)
	{
		m_keyUp = false;
	}
	if (key == GLUT_KEY_LEFT)
	{
		m_keyLeft = false;
	}
	if (key == GLUT_KEY_DOWN)
	{
		m_keyDown = false;
	}
	if (key == GLUT_KEY_RIGHT)
	{
		m_keyRight = false;
	}
}