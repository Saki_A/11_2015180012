#pragma once

#define MAX_OBJ_COUNT 1000

#define HERO_ID 0
#define Enemy_Boss 1

#define GRAVITY 9.8f


#define TYPE_HERO 0
#define TYPE_ENEMY 1
#define TYPE_BULLET 2
#define TYPE_NOMAL 3

#define Hero_MaxHP 100.f
#define Enemy_MaxHP 150

