#pragma once
#include "G_Object.h"
class Physics
{

public:
	Physics();
	~Physics();

	bool IsOverlap(G_Object* A, G_Object* B, int method = 0);
	void ProcessCollision(G_Object* A, G_Object* B);

private:
	bool BBOverlapTest(G_Object* A, G_Object* B);
};

